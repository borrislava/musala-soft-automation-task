package tests;

import java.sql.Timestamp;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ContactUsForm;

public class ContactUs extends BaseTest{
		String pageTitle = "Musala Soft";
		ContactUsForm contactUsForm;
		
	@Test
	public void verifyTitle() {	
		String actualTitle = driver.getTitle();
		Assert.assertEquals(actualTitle, pageTitle);
	}
	
	@Test
	public void openContactusForm() {
		landingPage.scrollDownToElement(landingPage.buttonContactUs);
		landingPage.applyCssToElement(landingPage.stickyHeader);
		landingPage.buttonContactUs.click();
	}
	
	@Test
	public void checkEmailErrorMessageAppears() {
		contactUsForm = new ContactUsForm(driver);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis()); 
		
		contactUsForm.waitUntilElementIsClickable(contactUsForm.contactFormEl);
		contactUsForm.inputName.sendKeys("testName" + timestamp);
		contactUsForm.inputEmail.sendKeys("testEmail" + timestamp);
		contactUsForm.inputSubmitBtn.sendKeys("testSubject" + timestamp);
		contactUsForm.textareaMessage.sendKeys("testMessage" + timestamp);
		contactUsForm.scrollDownToElement(contactUsForm.inputSubmitBtn);
		contactUsForm.inputSubmitBtn.click();
		Assert.assertTrue(contactUsForm.waitUntilElementIsClickable(contactUsForm.spanEmailErrorMessage));
		Assert.assertEquals(contactUsForm.spanEmailErrorMessage.getText(), "The e-mail address entered is invalid.");
	}
}