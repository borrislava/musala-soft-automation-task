package tests;

import java.time.Duration;
import java.util.Properties;
import java.io.*;
import pages.CommonElements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pages.LandingPage;

public class BaseTest {
	public WebDriver driver;
	public LandingPage landingPage;
	String browserName = "";
	String pageName = "";

	
	public WebDriver initializeDriver() {
		FileInputStream fis;
		Properties config = new Properties();  
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\resources\\config.properties");
			config.load(fis); 
		} catch (FileNotFoundException e) {
			System.out.println("Configuration file not found.");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Configuration file could not be loaded.");
			e.printStackTrace();
		}
				
		browserName = config.getProperty("browser");
		pageName = config.getProperty("page");
		if (browserName.equalsIgnoreCase("mozilla") || browserName.equalsIgnoreCase("firefox") || browserName.equalsIgnoreCase("mozilla firefox")) {
			//Mozilla
			System.setProperty("webdriver.firefox.marionette", "D:\\downloads\\webdrivers\\geckodriver.exe");
			driver = new FirefoxDriver();
		} else {
			//Chrome or another browser
			System.setProperty("webdriver.chrome.driver", "D:\\downloads\\webdrivers\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		return driver;
	}
	
	//intializes Webdriver and opens the landing page
	@BeforeClass
	public LandingPage launchApplication() {
		driver = initializeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.manage().window().maximize();
		landingPage = new LandingPage(driver);
		landingPage.goToPage(pageName);
		landingPage.btnAcceptAll.click();
		return landingPage;
	}
	
	@AfterClass
	public void closeBrowser() {
		driver.quit();
	}
}