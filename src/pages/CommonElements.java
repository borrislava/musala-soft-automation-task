package pages;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.JavascriptExecutor;

public class CommonElements {
	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor jse;
	
	public CommonElements(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		this.jse = (JavascriptExecutor) driver;
	}
	
	public String goToPage(String page) {	
		driver.get(page);
		return page;
	}
	
	public boolean waitUntilElementIsClickable(WebElement element) {
		WebElement elDisplayed = wait.until(ExpectedConditions.elementToBeClickable(element));
		if (elDisplayed == null)
			return false;
		else
			return true;
	}
	
	public void scrollDownToElement(WebElement el) {
		jse.executeScript("arguments[0].scrollIntoView(true);", el);;
	}
	
	public void applyCssToElement (WebElement element) {
		String script = "arguments[0].setAttribute('style', 'display: none')";
		jse.executeScript(script, element);
	}
	
}
