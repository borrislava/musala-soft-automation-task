package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactUsForm extends CommonElements{
	public String name = "testName";
	public String invalidEmail = "test invalid email";
	public String subject = "test subject";
	public String message = "test message";
	
	public ContactUsForm(WebDriver driver) {
		super(driver);
		PageFactory.initElements (driver, this);
	}
	
	@FindBy(id="contact_form_pop")
	public WebElement contactFormEl;
	
	@FindBy(css="input#cf-1") //span[data-name='your-name'] > input
	public WebElement inputName;
	
	@FindBy(css="span[data-name='your-email'] > input")
	public WebElement inputEmail;
	
	@FindBy(css="span[data-name='your-email'] > span")
	public WebElement spanEmailErrorMessage;
	
	@FindBy(css="span[data-name='your-subject'] > input")
	public WebElement inputSubject;
	
	@FindBy(css="span[data-name='your-message'] > textarea")
	public WebElement textareaMessage;
	
	@FindBy(css="input.btn-cf-submit[type='submit']")
	public WebElement inputSubmitBtn;
}
