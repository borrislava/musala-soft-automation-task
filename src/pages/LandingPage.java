package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.WebDriver;

public class LandingPage extends CommonElements {
	// WebDriver driver;
		
	public LandingPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements (driver, this);
	}

	@FindBy(id="wt-cli-accept-all-btn")
	public WebElement btnAcceptAll;
	
	@FindBy(css="header.sticky-header")
	public WebElement stickyHeader;
	
	// WebElement buttonContactUs = driver.findElement(By.className("contact-label"));
	@FindBy(className="contact-label")
	public WebElement buttonContactUs;
}
